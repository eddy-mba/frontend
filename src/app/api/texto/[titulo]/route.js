const url_search = process.env.DATABASE_URL_SEARCH;

export async function GET(request, { params }) {
  try {
    const response = await fetch(`${url_search}/${params.titulo}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return Response.json(data);
  } catch (error) {
    console.error(`Fetch GET error:`, error);
    throw error;
  }
}
