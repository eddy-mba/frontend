"use client";

import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, Container } from "@mui/material";
import { useState, useEffect } from "react";

export default function HomePage() {
  const [noticia, setNoticia] = useState([]);
  useEffect(() => {
    fetch(`/api`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setNoticia(data);
      });
  }, []);

  return (
    <Container>
      <div className="flex flex-col items-center space-y-4 min-h-screen">
        <h1 className="font-bold pt-4 text-blue-700">
          LAS NOTICIAS MAS IMPORTANTES EN TODA BOLIVIA
        </h1>

        <div className="flex flex-wrap justify-center">
          {noticia.map((obj) => (
            <Card
              className="m-2"
              sx={{ maxWidth: 345 }}
              key={obj.id}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="140"
                  // image="/static/images/cards/contemplative-reptile.jpg"
                  src={obj.imagen}
                  alt="Imagen noticias"
                />
                <CardContent>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div">
                    {obj.titulo}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary">
                    {obj.contenido}
                  </Typography>
                  <Typography>
                    <span className="text-end text-xs">
                      {obj.autor} | {obj.lugar} | {obj.fecha}
                    </span>
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          ))}
        </div>
      </div>
    </Container>
  );
}
