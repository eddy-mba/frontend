"use client";

import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, Container } from "@mui/material";
import { useState, useEffect } from "react";
import ButtonSend from "@/components/ButtonSend";
import { useRouter } from "next/navigation";

export default function Page() {
  const [noticia, setNoticia] = useState([]);

  const router = useRouter();

  useEffect(() => {
    fetch(`/api`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setNoticia(data);
      });
  }, []);

  function deleteData(params) {
    fetch(`/api/${params}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });
    location.reload();
  }
  return (
    <Container>
      <div className="flex flex-col items-center space-y-4 min-h-screen">
        <h1 className="font-bold pt-4 text-blue-700">
          ELIGE EL ARTICULO O NOTICIA QUE DESEAS{" "}
          <span className="text-red-500">ELIMINAR</span>
        </h1>
        <div className="flex flex-wrap justify-center">
          {noticia.map((obj) => (
            <Card
              className="m-2"
              sx={{ maxWidth: 345 }}
              key={obj.id}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="140"
                  src={obj.imagen}
                  alt="Imagen noticia"
                />
                <CardContent>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div">
                    {obj.titulo}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary">
                    {obj.contenido}
                  </Typography>
                  <Typography>
                    <span className="text-end text-xs">
                      {obj.autor} | {obj.lugar} | {obj.fecha}
                    </span>
                  </Typography>
                </CardContent>
              </CardActionArea>
              <div className="flex justify-center p-2">
                <ButtonSend
                  params={"ELIMINAR"}
                  onClick={() => deleteData(obj.id)}
                />
              </div>
            </Card>
          ))}
        </div>
      </div>
    </Container>
  );
}
