import Container from "@mui/material/Container";
import Formulario from "@/components/Formulario";

export default function Posts() {
  return (
    <Container>
      <div className="space-y-4 flex flex-col items-center min-h-screen">
        <h1 className="font-bold pt-4 text-blue-700">
          PUBLICAR TU ARTICULO O NOTICIA
        </h1>
        <Formulario />
      </div>
    </Container>
  );
}
