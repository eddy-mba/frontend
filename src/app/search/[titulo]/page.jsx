"use client";
import Container from "@mui/material/Container";
import { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import { CardActionArea } from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function SearchId({ params }) {
  const [noticia, setNoticia] = useState([]);

  useEffect(() => {
    fetch(`/api/texto/${params.titulo}`, {
      method: "GET",
      headers: {
        headers: {
          "Content-Type": "application/json",
        },
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
      })
      .then((data) => {
        setNoticia(data);
      });
  }, []);

  return (
    <Container>
      <div className="space-y-4 flex flex-col items-center min-h-screen">
        <h1 className="font-bold pt-4 text-blue-700">
          ARTICULOS O NOTICIAS QUE COINCIDEN CON TU BUSQUEDA
        </h1>
        <div className="flex flex-wrap justify-center">
          {noticia ? (
            noticia.map((obj) => (
              <Card
                className="m-2"
                sx={{ maxWidth: 345 }}
                key={obj.id}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="140"
                    // image="/static/images/cards/contemplative-reptile.jpg"
                    src={obj.imagen}
                    alt="Imagen noticias"
                  />
                  <CardContent>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div">
                      {obj.titulo}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="text.secondary">
                      {obj.contenido}
                    </Typography>
                    <Typography>
                      <span className="text-end text-xs">
                        {obj.autor} | {obj.lugar} | {obj.fecha}
                      </span>
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))
          ) : (
            <div>
              <h1 className="text-red-600">Busqueda no encontrada!!!!</h1>
            </div>
          )}
        </div>
      </div>
    </Container>
  );
}
