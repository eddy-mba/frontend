import Container from "@mui/material/Container";
import Formulario from "@/components/Formulario";

export default function page({ params }) {
  return (
    <Container>
      <div className="space-y-4 flex flex-col items-center min-h-screen">
        <h1 className="font-bold pt-4">EDITAR TU ARTICULO O NOTICIA</h1>
        <Formulario params={params} />
      </div>
    </Container>
  );
}
