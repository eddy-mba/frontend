import Button from "@mui/material/Button";
import SendIcon from "@mui/icons-material/Send";

export default function ButtonSend({ params, onClick = null, color = null }) {
  return (
    <Button
      size="small"
      type="button"
      className="bg-blue-600"
      variant="contained"
      endIcon={<SendIcon />}
      onClick={onClick}>
      {params}
    </Button>
  );
}
