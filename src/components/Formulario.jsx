"use client";

import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import { useForm } from "react-hook-form";
import { useRef } from "react";
import { useRouter } from "next/navigation";
import ButtonSend from "./ButtonSend";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";

export default function Formulario({ params }) {
  const { register, handleSubmit, setValue } = useForm();

  const formReset = useRef();

  const router = useRouter();

  if (params) {
    fetch(`/api/${params.id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setValue("titulo", data.titulo);
        setValue("imagen", data.imagen);
        setValue("autor", data.autor);
        setValue("lugar", data.lugar);
        setValue("contenido", data.contenido);
      });
  }

  const updateData = (body) => {
    if (params) {
      fetch(`/api/${params.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
      router.push("/edit");
      router.refresh();
    } else {
      fetch("/api", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });
    }
    formReset.current.reset();
  };
  return (
    <Box
      component="form"
      ref={formReset}>
      <Card className="w-[600px] space-y-4 py-4 px-8">
        <div>
          <InputLabel htmlFor="titulo">TITULO</InputLabel>
          <Input
            {...register("titulo")}
            id="titulo"
            fullWidth
            placeholder="Titulo de la noticia"
          />
        </div>
        <div>
          <InputLabel htmlFor="imagen">URL IMAGEN</InputLabel>
          <Input
            {...register("imagen")}
            id="imagen"
            fullWidth
            placeholder="https://media.gcflearnfree.org/content/5b1042136d5ad52ca4b6eb17_7_1_2014/mac-win-pc.png"
          />
        </div>
        <div>
          <InputLabel htmlFor="autor">AUTOR</InputLabel>
          <Input
            {...register("autor")}
            id="autor"
            fullWidth
            placeholder="Jorge Mendoza"
          />
        </div>
        <div>
          <InputLabel htmlFor="lugar">LUGAR</InputLabel>
          <Input
            {...register("lugar")}
            id="lugar"
            fullWidth
            placeholder="El Alto - La Paz"
          />
        </div>
        <div>
          <InputLabel htmlFor="contenido">CONTENIDO</InputLabel>
          <Input
            {...register("contenido")}
            id="contenido"
            fullWidth
            multiline
            rows={12}
            placeholder="Escribe el contenido tu articulo o noticia aqui."
          />
        </div>

        <div className="flex justify-center">
          <ButtonSend
            params={"ENVIAR"}
            onClick={handleSubmit(updateData)}
          />
        </div>
      </Card>
    </Box>
  );
}
