export default function Footer() {
  return (
    <div className="w-full h-20 mt-10 bg-blue-800 flex justify-center items-center">
      <p className="text-white">
        Desarrollado por Eddy Mamani <span className="font-bold">2024</span>
      </p>
    </div>
  );
}
