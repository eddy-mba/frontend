"use client";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import OtherHousesIcon from "@mui/icons-material/OtherHouses";
import PostAddIcon from "@mui/icons-material/PostAdd";
import EditNoteIcon from "@mui/icons-material/EditNote";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import { Container, Stack } from "@mui/material";
import Avatar from "@mui/material/Avatar";
import SearchIcon from "@mui/icons-material/Search";
import { useRouter } from "next/navigation";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import ButtonSend from "./ButtonSend";

import { useState } from "react";

import { useForm } from "react-hook-form";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    [theme.breakpoints.up("sm")]: {
      width: "20ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

export default function Navbar() {
  const [value, setValue] = useState(0);

  const router = useRouter();

  const { register, handleSubmit } = useForm();

  const pageBuscar = (body) => {
    if (body.titulo) {
      router.push(`/search/${body.titulo}`);
    }
  };

  return (
    <Container>
      <div className="flex justify-between border-b-2 border-blue-600">
        <div className="flex items-center space-x-2">
          <Stack
            direction="row"
            spacing={2}>
            <Avatar alt="Hocicon" />
          </Stack>
          <h1 className="text-blue-600 font-bold">EL HOCICON</h1>
        </div>
        <div className="flex items-center space-x-2">
          <div className="flex flex-col border border-blue-200  text-blue-500 w-64">
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Buscar por titulo"
                {...register("titulo")}
              />
            </Search>
          </div>
          <div>
            <ButtonSend
              params={"Buscar"}
              onClick={handleSubmit(pageBuscar)}
            />
          </div>
        </div>
        <div>
          <Box sx={{ width: 500 }}>
            <BottomNavigation
              showLabels
              value={value}
              onChange={(event, newValue) => {
                setValue(newValue);
              }}>
              <BottomNavigationAction
                sx={{ backgroundColor: "#cbd5e133" }}
                label="Inicio"
                icon={<OtherHousesIcon />}
                onClick={() => router.push("/")}
              />

              <BottomNavigationAction
                sx={{ backgroundColor: "#cbd5e133" }}
                label="Publicar"
                icon={<PostAddIcon />}
                onClick={() => router.push("/posts")}
              />

              <BottomNavigationAction
                sx={{ backgroundColor: "#cbd5e133" }}
                label="Editar"
                icon={<EditNoteIcon />}
                onClick={() => router.push("/edit")}
              />

              <BottomNavigationAction
                sx={{ backgroundColor: "#cbd5e133" }}
                label="Eliminar"
                icon={<DeleteOutlineIcon />}
                onClick={() => router.push("/delete")}
              />
            </BottomNavigation>
          </Box>
        </div>
      </div>
    </Container>
  );
}
