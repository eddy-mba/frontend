# Frontend El Hocicon

## Descripción del Proyecto

Este es el repositorio del frontend para el portal de noticias "El Hocicón".
Utiliza NextJS como framework de React y MUI (Material-UI) para la interfaz de usuario.
Se ha utilizado react-hook-form para capturar informacion del formulario.
Tambien se ha utilizado algunos fragmentos de codigo de Tailwind CSS.

## Tecnologías Utilizadas

- NodeJS v20
- NextJS v14
- MUI (Material-UI)
- React Hook Form
- Tailwind CSS

## Estructura del Proyecto

- `/src`: Contiene las carpetas app y components de NextJS.
- `/app`: Contiene las páginas de NextJS.
- `/components`: Contiene componentes reutilizables.
- `/styles`: Contiene estilos globales y temas personalizados.
- `/public`: Almacena archivos estáticos.
