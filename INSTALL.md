# Instrucciones de Instalación y Despliegue del Frontend

## Requisitos Previos

Asegúrate de tener las siguientes herramientas instaladas en tu máquina antes de proceder:

- [Node.js](https://nodejs.org/) (v20 o posterior)

## Instalación

1. **Clonar este repositorio:**

   ```bash
   git clone git@gitlab.com:eddy-mba/frontend.git

   ```

1. **Ingresar al directorio del frontend:**

   ```bash
   cd frontend

   ```

1. **Instalar las dependencias::**

   ```bash
   npm install

   ```

1. **Copiar el archivo de configuración:**
   - Copia el archivo de configuración .env.example a .env.
   - Abre el archivo .env y configura las variables de entorno según sea necesario.

## Instrucciones de Desarrollo

1. **Ejecutar la aplicación en modo de desarrollo:**

   ```bash
   npm run dev

   ```

   Abrir http://localhost:3000 en el navegador.

## Despliegue en vercel

La forma más sencilla de implementar la aplicación Next.js es utilizar la [Plataforma Vercel](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) de los creadores de Next.js.

Consulte la [documentación de implementación de Next.js](https://nextjs.org/docs/deployment) para obtener más detalles.
